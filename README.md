# koenigsmann

koeningsmann training app

[![Codemagic build status](https://api.codemagic.io/apps/5d8c85c56e88bb000f0a04af/5d8c85c56e88bb000f0a04ae/status_badge.svg)](https://codemagic.io/apps/5d8c85c56e88bb000f0a04af/5d8c85c56e88bb000f0a04ae/latest_build)

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
